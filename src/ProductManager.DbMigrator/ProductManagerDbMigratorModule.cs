﻿using ProductManager.EntityFrameworkCore;
using Volo.Abp.Autofac;
using Volo.Abp.Modularity;

namespace ProductManager.DbMigrator;

[DependsOn(
    typeof(AbpAutofacModule),
    typeof(ProductManagerEntityFrameworkCoreModule),
    typeof(ProductManagerApplicationContractsModule)
    )]
public class ProductManagerDbMigratorModule : AbpModule
{
}
