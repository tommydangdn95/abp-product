﻿using AutoMapper;
using ProductManager.Products;

namespace ProductManager;

public class ProductManagerApplicationAutoMapperProfile : Profile
{
    public ProductManagerApplicationAutoMapperProfile()
    {
        CreateMap<Product, ProductDto>();
    }
}
