﻿using System.Threading.Tasks;
using ProductManager.Localization;
using ProductManager.MultiTenancy;
using Volo.Abp.Identity.Web.Navigation;
using Volo.Abp.SettingManagement.Web.Navigation;
using Volo.Abp.TenantManagement.Web.Navigation;
using Volo.Abp.UI.Navigation;

namespace ProductManager.Web.Menus;

public class ProductManagerMenuContributor : IMenuContributor
{
    public async Task ConfigureMenuAsync(MenuConfigurationContext context)
    {
        if (context.Menu.Name == StandardMenus.Main)
        {
            await ConfigureMainMenuAsync(context);
        }
    }

    private Task ConfigureMainMenuAsync(MenuConfigurationContext context)
    {
        var administration = context.Menu.GetAdministration();
        var l = context.GetLocalizer<ProductManagerResource>();

        context.Menu
            .AddItem(
            new ApplicationMenuItem(
                "ProductManagement",
                l["Menu:ProductManagement"],
                icon: "fas fa-shopping-cart"
            )
            .AddItem( new ApplicationMenuItem(
                "ProductManagement.Products",
                l["Menu:Products"],
                url: "/Products")
            )
        );

        context.Menu.Items.Insert(
            0,
            new ApplicationMenuItem(
                ProductManagerMenus.Home,
                l["Menu:Home"],
                "~/",
                icon: "fas fa-home",
                order: 0
            )
        );

        if (MultiTenancyConsts.IsEnabled)
        {
            administration.SetSubItemOrder(TenantManagementMenuNames.GroupName, 1);
        }
        else
        {
            administration.TryRemoveMenuItem(TenantManagementMenuNames.GroupName);
        }

        administration.SetSubItemOrder(IdentityMenuNames.GroupName, 2);
        administration.SetSubItemOrder(SettingManagementMenuNames.GroupName, 3);

        return Task.CompletedTask;
    }
}
